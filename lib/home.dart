import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_appavailability/flutter_appavailability.dart';
import 'package:simple_permissions/simple_permissions.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Map<String, String>> installedApps;


  @override
  void initState() {
    super.initState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> getApps() async {
    List<Map<String, String>> _installedApps;

    if (Platform.isAndroid) {

      _installedApps = await AppAvailability.getInstalledApps();

    }


    setState(() {
      installedApps = _installedApps;
    });

  }

  @override
  Widget build(BuildContext context) {
    if (installedApps == null)
      getApps();

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Permission Manager'),
        ),
        body: Container(
          //padding: EdgeInsets.only(top: 10),
          child: ListView.builder(
          itemCount: installedApps == null ? 0 : installedApps.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(installedApps[index]["app_name"]),
              trailing: IconButton(
                icon: const Icon(Icons.open_in_new),
                onPressed: (){
                  SimplePermissions.openSettings();
                }                  
                //SimplePermissions.openSettings,
              ),
            );
          },
        )
        ),
      ),
    );
  }
}